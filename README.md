---
output:
  html_document: default
  pdf_document: default
---
# CLSI Guidelines

## Software
  - R version 3.4.0 (2017-04-21) -- "You Stupid Darkness"

## Installation

### 1. Java requiremnts (rJava module dependency)

  -   Source: (https://www.atlantic.net/community/howto/install-java-jre-jdk-on-ubuntu-16-04/)[]  
  - Check java version (linux terminal)  
```
    java -version 
```

  - Install Java Open JRE or JDK (OS: Ubuntu 16.04). once you have verified if Java is installed or not, choose the type of Java installation that you want with one the following:
```
    sudo apt install default-jdk
    sudo apt install default-jre
```
  - may need:
```
    sudo R CMD javareconf
```

### 2. Latex and Pandoc installation
#### Pandoc
  - Download package: https://github.com/jgm/pandoc/releases
  - Package file name: pandoc-1.19.2.1-1-amd64.deb
  - Install package via cli (in folder with installation package):
```
sudo dpkg -i pandoc-1.19.2.1-1-amd64.deb
```

#### TeXLive

  - Download package: https://tex.stackexchange.com/questions/134365/installation-of-texlive-full-on-ubuntu-12-04
  - Install package: 
```
sudo aptitude -y install texlive
sudo aptitude -y install texlive-latex-extra
```