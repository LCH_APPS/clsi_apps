// Define a function to run on page load.
var loadCheck = function() {


    // From here on, the page is loaded.
    // Obtain a list of all elements with the particular class name
    var elList = document.getElementsByClassName("htAutocomplete");
    console.log(elList)
    // Loop over the elements until there are no longer any with the class.
    while(elList.length > 0) {
        // For each element, remove the class.
        elList[0].className = elList[0].className.replace(
            /\bhtAutocomplete\b/,    // RegExp of class name in word boundaries
            ""                      // Replace with empty string - remove it.
        );
    }
};
loadCheck()

