library(shiny)
source('scripts/comunications_mod.R')
source('scripts/data_init.R')

# Module UI function
evalReport <- function(id) {
  # Create a namespace function using the provided id
  ns <- NS(id)
  tagList(
    tags$hr(),
    fluidRow(column(width = 3,
                    radioButtons(ns('final_qualification'), 'Calificación final:',
                                 c('Pasó' = 'pass',
                                   'Pasó con aclaraciones' = 'pass_clarify',
                                   'No pasó ' = 'no_pass')),
                    textAreaInput(ns('comment_eval'), 'Comentario sobre evaluación final'),
                    radioButtons(ns('signature'), 
                                 'Firma de director de laboratorio o designado',
                                 c('Germán Campuzano-Zuluaga' = 'gcampuzanozuluaga',
                                   'Natalia Guevara Arismendy' = 'nguevara',
                                   'Veronica Tangarife' = 'vtangarife'))),
             column(width = 3, 
                    fileInput(ns('support_files'), 'Adjuntar archivos de soporte', 
                              multiple = TRUE),
                    selectInput(ns('email_receipt'), 'Email', email_list, 
                                selected = c("gczuluaga@hematologico.com",
                                             "amontoya@hematologico.com",
                                             "calidad@hematologico.com",
                                             "nguevara@hematologico.com",
                                             "lablch@hematologico.com",
                                             "vtangarife@hematologico.com"), 
                                multiple = TRUE)
             )
    ),
    tags$hr(),
    actionButton(ns('create_report'), 
                 label = 'Finalizar y descargar reporte')
  )
}


evalReportIn <- function(input, output, session, 
                         experiment, calib, reagentdata, 
                         sample_data, values_claims, values_out, 
                         basic_values, result_data, rundata) {
  
  values_in <- reactiveValues()
  observe({
    print(result_data[['comment_precision']])
  })

  observeEvent(input$support_files, {
    if (is.null(values_in[['support_files']])) {
      values_in[['support_files']] <- list()
    }
    infile <- input$support_files
    if (is.null(infile)) {
      return()
    }
    else{
      for (i in 1:dim(infile)[1]) {
        nfile <- length(values_in[['support_files']]) + 1
        values_in[['support_files']][[nfile]] <- list(datapath = infile[i, 'datapath'],
                                                      filename = infile[i, 'name'])
        }
      }
    #print(values_in[['support_files']][[nfile]] )
    #file.copy(inFile$datapath, file.path(getwd(), inFile$name) )
    })
  
    observeEvent(input$create_report, {
    
    na_test <- apply(values_out[['DF']], 2, function(x) any(is.na(x)))
    
    na_test = FALSE
    
    
    if (na_test == TRUE) {
      
      return(NULL)
    }

    test <- basic_values$test
    instrument <- basic_values$instrument
    test_data_selected <- test_data[(test_data['test'] == test & test_data['instrument'] == instrument),]
    test_code <- test_data_selected[['code']]
    units <- test_data_selected[['units']]

    if (calib$calib_chk) {
      calibration_date = calib$calibration_date
      calibration_lot = calib$calib_lot
    }
    else{
      calibration_date = NULL
      calibration_lot = NULL
    }

    if(experiment == 1) {
      exp_specs <- list(
        exp_list = list(
           exp_intro = basic_values$anotaciones,
           claims_intro = values_claims[['claim_annot']],
           precision = result_data[[1]][['comment_precision']],
           bias = result_data[[2]][['comment_bias']],
           final_qual = input$comment_eval),
        exp_data = list(
           n_samples = values_out[['samples']],
           replicates = values_out[['replicates']],
           runs = values_out[['runs']],
           vendor_ref = values_claims[['data_src']],
           vendor_data_date = values_claims[['date_claims']],
           sample_type = sample_data[["SAMPLE_TABLE"]][1,2],
           precision_out = values_out[['precision_out']],
           bias_out = values_out[['bias_out']],
           vendor_df = values_claims[['CLAIMS']],
           sample_df = values_out[["SAMPLE_TABLE"]],
           run_df = rundata[["RUN_TABLE"]],
           outlier_df = values_out[['OUTLIERS']],
           precision_df = values_out[['precision_df']],
           claims_df = values_claims[['claims_df']],
           bias_df =  values_out[['bias_df']]),
        rmd_file = 'report_ep15.Rmd'
        )
      }
      if(experiment == 2) {
        exp_specs <- list(
          exp_list = list(
            exp_intro = basic_values$anotaciones,
            linearity = result_data[['comment_linearity']],
            final_qual = input$comment_eval),
          exp_data = list(
            dilution_description = sample_data[['dilution_description']],
            summary_df = result_data[['summary_df']],
            linereg_df = result_data[['linereg_df']]$fitted_mods,
            samples = values_out[['samples']],
            levels = values_out[['levels']],
            replicates = values_out[['replicates']],
            sample_type = sample_data[["SAMPLE_TABLE"]][1,2]),
          rmd_file = 'report_ep06.Rmd'
        )
      }
      if(experiment == 3) {
        exp_specs <- list(
          exp_list = list(),
          exp_data = list(),
          rmd_file = 'report_epxx.Rmd'
        )
      }
    
      comments_list <- exp_specs[['exp_list']]
      #CLEAN COMMENTS
      comments_list <- lapply(comments_list, function(x) gsub('\\s*$','.',x, perl=TRUE))
      comments_list <- lapply(comments_list, function(x) gsub('\\.*$','.',x))
      
      version <- '1.0.0'
      app_name <- experiment
      
      signature_paths <- list(gcampuzanozuluaga = list(name = 'Germán Campuzano-Zuluaga', 
                                                       title = 'Director Médico',
                                                       path = 'gcz_sign.jpg'),
                              nguevara = list(name = 'Natalia M. Guevara Arismendy',
                                                    title = 'Coordinación Control y Aseguramiento de Calidad',
                                                    path = 'nga_sign.jpg'),
                              gescobargallo = list(name = 'Veronica J. Tangarife Castaño', 
                                                   title = 'Coordinación del Laboratorio',
                                                   path = 'vtc_sign.jpg'))
      signed = signature_paths[[input$signature]]

      # Create json string
      exp_data_out <- list(
        local = FALSE,
        version = version,
        app_name = app_name,
        signed = signed,
        date = Sys.Date(),
        date_in = basic_values$date_range[[1]],
        date_out = basic_values$date_range[[2]],
        test = test,
        test_code = test_code,
        units = units,
        instrument = instrument,
        responsable_exp = basic_values$responsable,
        calibration_date = calibration_date,
        calibration_lot = calibration_lot,
        comments = comments_list,
        final_qual = input$final_qualification,
        exp_df = values_out[["DF"]],
        sample_df = sample_data[["SAMPLE_TABLE"]],
        reagent_df = reagentdata[["reagent_table"]]
      )
      exp_data_out <- c(exp_data_out, exp_specs[['exp_data']])
      exp_folder <- paste(gsub('[^[:alnum:]][(\\sc|\\sb)]', '_', test), 
                          gsub('[- :]', '', as.character(Sys.time())), 
                          sep = '_')
      pdf_name <- paste(exp_folder, '.pdf', sep = '')
      support_path <- file.path(getwd() , 'tests', exp_folder, 'documentos de soporte')
      output_file <- file.path(getwd(), 'tests', exp_folder, pdf_name)
      json_path <- file.path(getwd() , 'tests', exp_folder, 'experiment_data.json')
      dir.create(file.path(getwd(), 'tests', exp_folder), 
                 showWarnings = FALSE)
      dir.create(support_path, 
                 showWarnings = FALSE)
      
      if (!is.null(values_in[['support_files']])) {
        for (sup_file in values_in[['support_files']]) {
          file.copy(sup_file$datapath, 
                    file.path(support_path, sup_file$filename) )
        }
      }
      # Save data
      write(toJSON(exp_data_out, pretty = TRUE), json_path)
      params <- list(json_path = json_path)
      rmd_path <- file.path(getwd(), 'reporting', exp_specs[['rmd_file']], 
                            fsep = .Platform$file.sep)
      rmarkdown::render(rmd_path, output_file = 'temp_out.pdf',
                        params = params,  envir = new.env(parent = globalenv()))
      file.copy('reporting/temp_out.pdf', output_file)
      file.remove('reporting/temp_out.pdf')
      to_list <- input$email_receipt
      print(to_list)
      smtp_data <- list(smtp = "smtp.gmail.com",
                        port = 587,
                        username = "gczuluaga@hematologico.com",
                        passwd = ""
                        )
      send_report(test, test_code, output_file, to_list, smtp_data)
  })

}
