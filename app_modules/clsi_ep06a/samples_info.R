library(shiny)
library(rhandsontable)

source('scripts/data_init.R')
source('scripts/general_fx.R')

# Module UI function
sampleInfo <- function(id) {
  # Create a namespace function using the provided id
  ns <- NS(id)
  tagList(
    HTML('<label class="control-label" for="units">Tabla 3. Datos de las muestras</label>'),
    rHandsontableOutput(ns('sample_table')),
    textAreaInput(ns('dilution_description'), 'Descripción de método para diluciones')
  )
}

sampleInfoIn <- function(input, output, session, experiment, ins) {
  values_in <- reactiveValues() # this is the session variable equivalent
  ## session variable (values) processing
  observe({
    
    values_in[['replicates']] <- as.numeric(ins$n_replicates)
    values_in[['samples']] <- as.numeric(ins$n_samples)
    values_in[['dilution_description']] <- input$dilution_description
    
    if (!is.null(input$sample_table)) {
      sample_data <- hot_to_r(input$sample_table)
    }
    else {
      if (is.null(values_in[["SAMPLE_TABLE"]])){
        sample_data <- intial_samples(experiment)
      }
      else{
        sample_data <- values_in[["SAMPLE_TABLE"]]
      }
    }
    values_in[["SAMPLE_TABLE"]]<-sample_data
    values_in
  })
  
  output$sample_table <- renderRHandsontable({
    
    DF <- values_in[["SAMPLE_TABLE"]]
    new_row_lg_df <- values_in[['samples']]
    nrow_df <- dim(DF)[1]
    new_cols <- FALSE
    df_colnames <-c('Muestra', 'Tipo', 'Proveedor', 'Lote', 'Fecha de vencimiento', 'Valor')
    DF <- mod_table(DF, new_row_lg_df, nrow_df, length(df_colnames), length(df_colnames), df_colnames, new_cols)
    colnames(DF)<-df_colnames
    DF[['Muestra']]<- c( 1:values_in[['samples']])
    rhandsontable(DF, useTypes = TRUE,   stretchH = "all" , rowHeaders=NULL)%>%
      hot_col(col = "Muestra", type = "autocomplete",  strict = FALSE, readOnly=TRUE)%>%
      hot_col(col = "Tipo", type = "dropdown", source = sample_origin, strict = FALSE)%>%
      hot_col(col = "Proveedor", type = "autocomplete",  strict = FALSE)%>%
      hot_col(col = "Lote", type = "autocomplete",  strict = FALSE)%>%
      hot_col(col = 'Fecha de vencimiento', type = "date", strict = FALSE)%>%
      hot_col(col = "Valor", type = "numeric", format = '0.0', strict = FALSE)%>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
    
  })
  
  return(values_in)
  
}
