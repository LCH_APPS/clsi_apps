#auth: gcz 2016

library(ggplot2)
library(reshape2)
library(outliers)


##########################
###### ANALYSIS ###### 

## Run Grubb's test for outliers on each sample, 
  ##Crit. val set to 0.9949906 as per CLSI guide
grubbs_proc <- function(df){
  df_outliers <- df[,c(1:4)]
  df_test <- df[,c(seq(3,dim(df)[2]))]
  grubbs_critical <- qgrubbs(0.9949906, 
                             dim(df)[1], 
                             type = 10, 
                             rev = FALSE)
  outlier_cols <- list()
  for (col in names(df_test)){
    val_vect = df_test[[col]]
    grubb_lim = mean(val_vect) + c(-1,1) * (grubbs_critical * sd(val_vect))
    logic_arr <- sapply(val_vect, 
                        function(x) (x < grubb_lim[[1]] | x > grubb_lim[[2]]))
    if (TRUE %in% logic_arr){
      positions_outliers = which(logic_arr == TRUE, arr.ind=TRUE)
      outlier_cols[[col]] <- positions_outliers
      df_outliers[[paste(c(col,'*'), collapse='')]] <- df_test[[col]]
      df_outliers[[paste(c(col,'*'), collapse='')]][positions_outliers]<- NA
      }
    }
  if (dim(df_outliers)[2] > 4){
    if (length(positions_outliers) > 1){
      return('invalid_exp')
      
    }
    else{
      out_data <- as.data.frame(df_outliers[,c(5:dim(df_outliers)[2])])
      colnames(out_data) <- colnames(df_outliers)[5:dim(df_outliers)[2]]
      return (out_data)
    }
    }
  else{
    return(NULL)
    }
  }

## Recalculate N for samples with outliers
recalc_n <- function(df){
  df <- na.exclude(df)
  collapsed_run <- acast(df, Corrida~colnames(df[2]))
  k = length(collapsed_run)
  n <- sum(collapsed_run)
  sn2 = sum(sapply(collapsed_run, function(x) x^2))
  n_zero <- (n-(sn2/n))/(k-1)
  return(n_zero)
}

## Get summary stats and put results in table
summary_stats <- function(df){
  summary_tab <- data.frame(row.names = c('N', 'X_N', 'SD', 'CV', 'Min.', 'Max.', 'Grubbs_{min}', 'Grubbs_{max}'))
  n_samples <- dim(df)[2]
  grubbs_critical <- qgrubbs(0.9949906, 
                             dim(df)[1], 
                             type = 10, 
                             rev = FALSE)
  for (i in 3:(n_samples)) {
    sample_vect <- na.omit(df[[i]])
    grubb_lim = mean(sample_vect) + c(-1,1) * (grubbs_critical * sd(sample_vect))
    in_col <- c(length(sample_vect), 
                mean(sample_vect), 
                sd(sample_vect),
                (sd(sample_vect)/mean(sample_vect))*100,
                min(sample_vect),
                max(sample_vect), 
                grubb_lim[1], 
                grubb_lim[2])
    summary_tab[[colnames(df[i])]] = round(in_col, 6)
  }
  return(summary_tab)
}

## Run ANOVA and put results in table
one_aov <- function(df){
  df$Corrida <- as.factor(df$Corrida)
  fitted_mods <- list()
  n_samples <- dim(df)[2]
  summary_tab <- data.frame(row.names = c('MS_{between}', 'MS_{within}', 
                                          'n_0', 'V_{between}', 'V_{within}', 
                                          'S_r', 'CV_r','S_{wl}','CV_{wl}'))
  for (i in 3:(n_samples)) {
    sample_df <- data.frame(df$Corrida, as.double(df[[i]]))
    colnames(sample_df) <- c('Corrida', colnames(df[i]))
    sample_df <- na.exclude(sample_df)
    fit <- aov(sample_df[[colnames(df[i])]] ~ Corrida, data=sample_df) 
    s_fit <- summary(fit)
    fitted_mods[[i-2]] = list(fit = fit, summary = summary(fit), 
                              hist = ggplot(df,aes(x=df[[i]]))+geom_density()+facet_grid(~Corrida)+theme_bw())
    ms1 <- s_fit[[1]][[3]][[1]]
    ms2<- s_fit[[1]][[3]][[2]]
    n0 <- recalc_n(sample_df)
    if (ms1 <= ms2){
      vb <- 0
    }
    else{
      vb <- (ms1-ms2)/n0 
    }
    vw <- ms2
    sr <- sqrt(vw)
    sb <- sqrt(vb)
    swl <- sqrt(vw + vb)
    sample_vect <- sample_df[[colnames(df[i])]]
    in_col <- c(ms1, ms2, n0, vb, vw,
                sr, (sr/mean(sample_vect))*100,
                swl, (swl/mean(sample_vect))*100)
    summary_tab[[colnames(df[i])]] = round(in_col, 6)
  }
  return(summary_tab)
}

## WL degrees of freedom
degfreed_wl_calc <- function(cv_r, rho, mean, n, n0, runs){
  # mean <- 1
  # cv_r <- 1
  cv_wl <- rho*cv_r
  v_w <- ((cv_r * mean)/100)^2
  v_wl <- ((cv_wl * mean)/100)^2
  v_b <- v_wl - v_w
  ms1 <- v_w + n0 * v_b
  ms2 <- v_w
  degfree1 <- runs - 1
  degfree2 <- n - runs  
  a1 <- 1/n0
  a2 <- (n0-1)/n0
  num <- ((a1*ms1) + (a2*ms2))^2
  den1 <- ((a1*ms1)^2)/degfree1
  den2 <- ((a2*ms2)^2)/degfree2
  degfree_wl <- num/(den1 + den2)
  return(degfree_wl)
}

## Claims UVL
get_uvl <- function(claims_df, runs, summary_table){
  n <- summary_table['N',1]
  nsamples <- length(names(summary_table))
  degfree_r <- n - runs
  prob <- 1-0.05/nsamples
  
  f <- function(x){
    n0<-as.numeric(summary_table['N', paste0('muestra.', x['Muestra fabricante'], collapse='')])
    cv_r = as.numeric(x['CV r'])
    mean = as.numeric(x['Media'])
    rho = as.numeric(x['SD wl'])/as.numeric(x['SD r'])
    degfree_wl <- degfreed_wl_calc(cv_r, rho, mean, n, n0, runs)
    critical_val_r <- sqrt(qchisq(prob,degfree_r)/degfree_r)
    critical_val_wl <- sqrt(qchisq(prob,degfree_wl)/degfree_wl)
    return(list(round(critical_val_r*as.numeric(x['CV r']),6), 
                round(critical_val_wl*as.numeric(x['CV wl']), 6)))
  }
  uvls <- apply(claims_df, 1, f)
  return(uvls)
}

###### END_ANALYSIS ###### 
##########################

###################
###### PLOTS ###### 

  ## get all plots in a list; this is called from server
get_plots <- function(df, analyte, units){
  plot_list <- list()
  
  #ylab <- paste0(analyte, ' (', units, ')') 
  ylab <- ''
  
  for (sample in colnames(df[,c(3:dim(df)[2])])){
    sub_df <- data.frame(as.factor(df$Corrida), df[[sample]])
    colnames(sub_df) <- c('Corrida', sample)
    entire_sample_bp <- entire_bp(sub_df, sample)
    entire_sample_bp <- entire_sample_bp + labs(x="Total",y=ylab)
    
    runs_bp <- run_bp(sub_df, sample)
    runs_bp <- runs_bp + labs(x="Corridas",y=ylab)
    plot_list[[sample]][['sample']] <- entire_sample_bp
    plot_list[[sample]][['runs']] <- runs_bp
    plot_list[[sample]][['title']] <- sample
  }
  return(plot_list)
  }

  ## get boxplot for entire sample
entire_bp <- function(sub_df, sample){
  p <- ggplot(sub_df, 
              aes(y=sub_df[[sample]],x=1)) + 
    geom_boxplot(outlier.colour="red", 
                 outlier.shape=8,
                 outlier.size=4) +
    geom_jitter(position=position_jitter(width=.1, height=0.002), col='#2c4381', alpha = 0.7) +
    stat_summary(fun.y=mean, 
                 geom="point", 
                 shape=23, 
                 size=4) + 
    stat_boxplot(geom ='errorbar',
                 width = 0.5) +
    theme(axis.text.x = element_blank()) + 
    theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(),
                       panel.grid.minor = element_blank(), axis.line = element_line(colour = "grey"))
  print(p)
  return(p)
  }

  ## get boxplots for each run in a sample
run_bp <- function(sub_df, sample){
  p <- ggplot(sub_df, 
              aes(x=Corrida, y=sub_df[[sample]])) + 
    geom_boxplot(outlier.colour="red", 
                 outlier.shape=8,
                 outlier.size=4) +
    geom_jitter(position=position_jitter(width=.1, height=0.002), col='#2c4381', alpha = 0.7) +
    stat_summary(fun.y=mean, 
                 geom="point", 
                 shape=23, 
                 size=4) + 
    stat_boxplot(geom ='errorbar', 
                 width = 0.5)+
    theme(axis.text.x = element_blank()) + 
    theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(),
                       panel.grid.minor = element_blank(), axis.line = element_line(colour = "grey"))
  print(p)
  return(p)
  }

###### END_PLOTS ###### 
##########################

###################
###### TESTS ###### 
test_fx <- function(){
  recalc_n_ful <- function(df, outlier_list){
    for (col in names(outlier_list)){
      df_minus <- df[-outlier_list[[col]],]
      collapsed_run <- acast(df_minus, run~col)
      k = length(collapsed_run)
      n <- sum(collapsed_run)
      sn2 = sum(sapply(collapsed_run, function(x) x^2))
      n_zero <- (n-(sn2/n))/(k-1)
    }
    return(n_zero)
  }
  
  test_outliers <- grubbs_proc(df)
  aov_data <- one_aov(df)
  grubs_test = grubbs.test(df$sample2, type = 10, opposite = FALSE, two.sided = TRUE)
  grubs_test
  lower_limit <- mean(df$sample2)+(grubs_test$statistic[[1]]*sd(df$sample2))
  lower_limit
  qgrubbs(0.9949906, 25, type = 10, rev = FALSE)
  pgrubbs(3.135, 25, type = 10)
  #grubs_test = grubbs.test(x, type = 10, opposite = FALSE, two.sided = TRUE)
  #return(grubs_test.p.value)
  html_str <- '<div>'
  for (e in aov_data) {
    html_str <- paste(c(html_str,
                        xtable(e[['summary']][[1]][1:4], type='html')), 
                      collapse='')
  }
  html_str <- paste(c(html_str,'</div>'), 
                    collapse='')
}
###################